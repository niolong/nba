﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NBA
{
    class Program
    {
        struct NBA
        {
            public static int CURRENT_id = 0;

            public int id;
            public string FIO;
            public string team;
            public int age;
            public int size;
            public double reiting;
        }

        #region Resize Method
        static void ResizeArra(ref NBA[] teams, int newLenght)
        {
            int minLenght = newLenght > teams.Length ? teams.Length : newLenght; ;

            NBA[] newArray = new NBA[newLenght];

            for (int i = 0; i < minLenght; i++)
            {
                newArray[i] = teams[i];
            }

            teams = newArray;
        }
        #endregion

        #region System Method
        static int InputInt(string msg)
        {
            bool result = true;
            int num;

            do
            {
                Console.WriteLine(msg);
                result = int.TryParse(Console.ReadLine(), out num);
            } while (!result);

            return num;
        }

        static double InputDouble(string msg)
        {
            bool result = true;
            double a;

            do
            {
                Console.WriteLine(msg);
                result = double.TryParse(Console.ReadLine(), out a);
            } while (!result);

            return a;
        }

        static string InputString(string msg)
        {
            Console.WriteLine(msg);
            return Console.ReadLine();
        }

        static bool InputBool(string msg)
        {
            bool inputResult;
            bool b;

            do
            {
                Console.Write(msg);
                inputResult = bool.TryParse(Console.ReadLine(), out b);
            } while (inputResult == false);

            return b;
        }
        #endregion

        #region CRUD Method
        static void AddNewTeam(ref NBA[] teams, NBA team)
        {
            if (teams == null)
            {
                teams = new NBA[1];
            }
            else
            {
                ResizeArra(ref teams, teams.Length + 1);
            }
            teams[teams.Length - 1] = team;
        }
        #endregion

        #region PrintMethod
        static void Print(NBA team)
        {
            Console.WriteLine("{0,2}{1,7}{2,12}{3,5}{4,5}{5,5}", team.id, team.FIO, team.team, team.age, team.size, team.reiting);
        }

        static void PrintAll(NBA[] teams)
        {
            if (teams == null)
            {
                Console.WriteLine("Массив пуст");
            }
            else if (teams.Length == 0)
            {
                Console.WriteLine("Массив пуст");
            }
            else
            {
                Console.WriteLine("{0,2}{1,7}{2,12}{3,5}{4,5}{5,5}", "id", "ФИО", "Команда", "Возраст", "Рост", "Рейтинг");

                for (int i = 0; i < teams.Length; i++)
                {
                    Print(teams[i]);
                }
            }

            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
        }

        static void PrintAllTeam(NBA[] teams, string fileName)
        {
            StreamWriter writet = new StreamWriter(fileName);

            writet.WriteLine("{0,2}{1,7}{2,12}{3,5}{4,5}{5,5}", "id", "ФИО", "Команда", "Возраст", "Рост", "Рейтинг");

            if (teams == null)
            {
                Console.WriteLine("Массив пуст");
            }
            else if (teams.Length == 0)
            {
                Console.WriteLine("Массив пуст");
            }
            else
            {

                for (int i = 0; i < teams.Length; i++)
                {
                    writet.WriteLine("{0,2}{1,7}{2,12}{3,5}{4,5}{5,5}", teams[i].id, teams[i].FIO, teams[i].team, teams[i].age, teams[i].size, teams[i].reiting);
                }
            }

            writet.Close();
        }
        #endregion

        #region Thool Method

        static double GetMinReit(NBA[] teams)
        {
            double minReit = teams[0].reiting;

            for (int i = 0; i < teams.Length; i++)
            {
                if (teams[i].reiting < minReit)
                {
                    minReit = teams[i].reiting;
                }
            }

            return minReit;
        }

        static double GetMaxReit(NBA[] teams)
        {
            double maxReit = teams[0].reiting;

            for (int i = 0; i < teams.Length; i++)
            {
                if (teams[i].reiting > maxReit)
                {
                    maxReit = teams[i].reiting;
                }
            }

            return maxReit;
        }

        static NBA CreateTeam(bool isNewId)
        {
            NBA team;

            if (isNewId)
            {
                NBA.CURRENT_id++;
                team.id = NBA.CURRENT_id;

            }
            else
            {
                team.id = 0;
            }

            team.FIO = InputString("Введите ФИО игрока: ");
            team.team = InputString("Введите название команды: ");
            team.age = InputInt("Введите возраст: ");
            team.size = InputInt("Введите рост: ");
            team.reiting = InputDouble("Введите рейтинг: ");

            return team;
        }

        static NBA CreateEmptyTeam()
        {
            NBA team;
            team.id = 0;
            team.FIO = "";
            team.team = "";
            team.age = 0;
            team.size = 0;
            team.reiting = 0.0;

            return team;
        }

        static NBA[] FindTeamfromMinToMaxReit(NBA[] teams, double maxReit, double minReit)
        {
            NBA[] findedReit = null;

            for (int i = 0; i < teams.Length; i++)
            {
                if (teams[i].reiting >= minReit && teams[i].reiting <= maxReit)
                {
                    AddNewTeam(ref findedReit, teams[i]);
                }
            }

            return findedReit;
        }

        static NBA[] FindTeam(NBA[] teams)
        {
            NBA[] find = null;

            string FindTeam = InputString("Введите команду");

            for(int i=0;i<teams.Length;i++)
            {
                if(teams[i].team==FindTeam)
                {
                    AddNewTeam(ref find, teams[i]);
                }
            }

            return find;
        }
      
        #endregion

        #region Sort
        static void SortAge(NBA[] teams, bool asc)
        {
            NBA temp;
            bool sort;
            int offset = 0;

            do
            {
                sort = true;

                for (int i = 0; i < teams.Length - 1 - offset; i++)
                {
                    bool compareResult;

                    if (asc)
                    {
                        compareResult = teams[i + 1].age < teams[i].age;
                    }
                    else
                    {
                        compareResult = teams[i + 1].age > teams[i].age;
                    }
                    if (compareResult)
                    {
                        temp = teams[i];
                        teams[i] = teams[i + 1];
                        teams[i + 1] = temp;

                        sort = false;
                    }
                }

                offset++;
            } while (!sort);
        }
        #endregion

        #region Interface 
        static void PrintMenu()
        {
            Console.WriteLine("0. Выход");
            Console.WriteLine("1. Добавить игрока");
            Console.WriteLine("2. Поиск игроков по рейтингу");
            Console.WriteLine("3. Сортировка повозрасту");
            Console.WriteLine("4. Печать");
            Console.WriteLine("5. Поиск по команде");
        }
        #endregion

        static void Main(string[] args)
        {
            NBA[] teams = null;
            bool runProg = true;

            while (runProg)
            {
                Console.Clear();

                PrintAll(teams);

                PrintMenu();
                int menuPoint = InputInt("Введите пункт меню: ");

                switch (menuPoint)
                {
                    case 0:
                        {
                            runProg = false;
                            Console.ReadKey();
                        }
                        break;

                    case 1:
                        {
                            NBA team = CreateTeam(true);
                            AddNewTeam(ref teams, team);
                        }
                        break;

                    case 2:
                        {
                            Console.WriteLine($"Введите значение от {GetMinReit(teams)} до {GetMaxReit(teams)}: ");

                            double maxReit = InputDouble("Введите максимальный: ");
                            double minReit = InputDouble("Введите минимальный: ");

                            NBA[] findedReit = FindTeamfromMinToMaxReit(teams, maxReit, minReit);

                            PrintAll(findedReit);
                        }
                        break;

                    case 3:
                        {
                            bool asc = InputBool("Введите asc или desc(true of false): ");
                            SortAge(teams, asc);
                        }
                        break;

                    case 4:
                        {
                            string fileName = InputString("Введите название файла: ");

                            PrintAllTeam(teams, fileName);
                        }
                        break;

                   case 5:
                        {
                            NBA[] findedTeam = FindTeam(teams);

                            PrintAll(findedTeam);
                        }
                        break;

                    default:
                        {
                            Console.WriteLine("Неизвестная команда, повторите ввод");
                            Console.ReadKey();
                        }
                        break;
                }
                Console.ReadKey();
            }
          }
       }
}
